# README #

This sample project uses SQLite to persist data and display on UITableView on second page.

This basic model was created to compare SQLite to use CoreData, please see example of CoreDataExample, which has this model using CoreData instead.

(c) Jim Tierney 2015