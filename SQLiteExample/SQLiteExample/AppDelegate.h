//
//  AppDelegate.h
//  SQLiteExample
//
//  Created by Jim Tierney on 13/01/2015.
//  Copyright (c) 2015 apjam ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

