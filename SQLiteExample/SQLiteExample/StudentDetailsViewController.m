//
//  StudentDetailsViewController.m
//  SQLiteExample
//
//  Created by Jim Tierney on 13/01/2015.
//  Copyright (c) 2015 apjam ltd. All rights reserved.
//

#import "StudentDetailsViewController.h"
#import "DBManager.h"

@interface StudentDetailsViewController ()

@end

@implementation StudentDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}



- (IBAction)findDataButtonPressed:(id)sender {
    NSArray *data = [[DBManager getSharedInstance]findByRegisterNumber:self.findByRegNumberTextField.text];
    if (data==nil) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Data not found"
                                                       message:nil
                                                      delegate:nil
                                             cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        self.regNumberTextField.text = @"";
        self.nameTextField.text = @"";
        self.departmentTextField.text = @"";
        self.yearTextField.text = @"";
    }else{
        self.regNumberTextField.text = self.findByRegNumberTextField.text;
        self.nameTextField.text = [data objectAtIndex:0];
        self.departmentTextField.text = [data objectAtIndex:1];
        self.yearTextField.text = [data objectAtIndex:2];
    }
    
    
    [self.view endEditing:YES];
}

- (IBAction)saveDataButtonPressed:(id)sender {
    
    BOOL success = NO;
    NSString *alertString = @"Data Insertion failed";
    if (self.regNumberTextField.text.length>0 &&self.nameTextField.text.length>0 &&self.departmentTextField.text.length>0 &&self.yearTextField.text.length>0) {
        success = [[DBManager getSharedInstance]saveData:self.regNumberTextField.text
                                                 name:self.nameTextField.text
                                           department:self.departmentTextField.text
                                                 year:self.yearTextField.text];
    }else{
        alertString = @"Enter all fields";
    }
    
    if (!success) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:alertString
                                                       message:nil
                                                      delegate:nil
                                             cancelButtonTitle:@"OK"
                                             otherButtonTitles:nil, nil];
        [alert show];
    }

}

#pragma mark - Textfield delegate methods

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField{

    if (textField==self.yearTextField||textField==self.departmentTextField) {
        
        self.horizontalAlighnmentConstraint.constant += 70;
        [UIView animateWithDuration:0.3 animations:^{
            [self.view layoutIfNeeded];
        }];
    }
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    if (textField==self.yearTextField||textField==self.departmentTextField) {
        self.horizontalAlighnmentConstraint.constant -= 70;
        [UIView animateWithDuration:0.3 animations:^{
            [self.view layoutIfNeeded];
        }];
    }
}
@end
