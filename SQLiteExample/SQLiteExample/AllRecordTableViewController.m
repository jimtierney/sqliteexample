//
//  AllRecordTableViewController.m
//  SQLiteExample
//
//  Created by Jim Tierney on 13/01/2015.
//  Copyright (c) 2015 apjam ltd. All rights reserved.
//

#import "AllRecordTableViewController.h"
#import "DBManager.h"

@interface AllRecordTableViewController ()

@property (strong, nonatomic) NSArray *studentRecordsArray;

@end

@implementation AllRecordTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.tableView reloadData];
    // Do any additional setup after loading the view.
}

-(NSArray*)studentRecordsArray{
    
    NSArray *records = [[DBManager getSharedInstance]allStudentRecords];
    
    self.studentRecordsArray = records;
    
    return _studentRecordsArray;
}

#pragma mark UITableview delegate methods

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.studentRecordsArray count];
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CellID" forIndexPath:indexPath];
    
    cell.textLabel.text = [[self.studentRecordsArray objectAtIndex:indexPath.row]objectForKey:@"regNumber"];
    cell.detailTextLabel.text = [[self.studentRecordsArray objectAtIndex:indexPath.row]objectForKey:@"name"];
    
    return cell;
}


@end
